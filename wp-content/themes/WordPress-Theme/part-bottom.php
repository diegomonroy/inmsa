<!-- Begin Bottom -->
	<section class="bottom" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'bottom' ); ?>
				<?php get_template_part( 'part', 'copyright' ); ?>
			</div>
		</div>
	</section>
<!-- End Bottom -->